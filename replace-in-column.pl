#!/usr/bin/perl

# Replace text in a column.

# 2019-09-26 sedwards

# ttd
#
# --leet - include numbers in 'from' regexes
#	o = 0
#	l = 1 (or i)
#	z = 2
#	e = 3
#	a = 4
#	s = 5
#	b = 6
#	t = 7
#	g = 9

# perldb arguments
# --host=db10 --user=sedwards --password=test --database=test --table=sedwards --id-column=my_id --text-column=foo

use	5.010;
use	strict;
use	warnings;

use	DBI;
use	Data::Dumper;
use	Getopt::Long;
use	POSIX qw(strftime);
use	POSIX;

use	constant			bs	=> '\\';	# backslash
use	constant			bt	=> '`';		# backtick
use	constant			dq	=> '"';		# double-quote
use	constant			nl	=> "\n";	# newline
use	constant			nlx2	=> "\n" x 2;	# 2 newlines
use	constant			sq	=> "'";		# single quote
use	constant			tab	=> "\t";	# tab
use	constant			tabx2	=> "\t" x 2;	# 2 tabs
use	constant			tabx3	=> "\t" x 3;	# 3 tabs
use	constant			tabx4	=> "\t" x 4;	# 4 tabs
use	constant			tabx5	=> "\t" x 5;	# 5 tabs

my $dbh;
my $debug;
my $select;
my $ssh;
my $update;
my $ush;
my $verbose;
my @row;

sub					examples
	{
        print nl
		. tab . './replace-in-column.pl' . bs . nl
		. tabx2 . '--host=localhost' . bs . nl
		. tabx2 . '--user=${user}' . bs . nl
		. tabx2 . '--password=${password}' . bs . nl
		. tabx2 . '--database=neil' . bs . nl
		. tabx2 . '--table=ProducerClipSite' . bs . nl
		. tabx2 . '--id-column=SiteID' . bs . nl
		. tabx2 . '--text-column=Description' . bs . nl
		. tabx2 . '--select-size=2000' . bs . nl
		. tabx2 . '--rlike=' . sq . 'make love not war|mlnw' . sq . bs . nl
		. tabx2 . '--regex=' . sq . 'make love not war:making love' . sq . bs . nl
		. tabx2 . '--regex=' . sq . 'mlnw:ml' . sq . bs . nl
		. tabx2 . '--word-match' . bs . nl
		. tabx2 . '--update-size=50' . bs . nl
		. tabx2 . '--sleep=2' . bs . nl
		. tabx2 . '--verbose' . bs . nl
		. tabx2 . '--dry-run' . nl
		. nl
		. 'Select rows from the ProducerClipSite table, ordered by SiteID in' . nl
		. 'chunks of 2000, starting with the minimum value of SiteID, that' . nl
		. 'contain ' . sq . 'make love not war' . sq . ' or ' . sq . 'mlnw' . sq . '.' . nl
		. nl
		. 'For each row found, replace ' . sq . 'make love not war' . sq . ' with ' . sq . 'making love' . sq . nl
		. 'and replace ' . sq . 'mlnw' . sq . ' with ' . sq . 'ml' . sq . ', only matching whole words. Note that' . nl
		. 'case will be preserved, hopefully in a meaningful way.' . nl
		. nl
		. 'Sleep for 2 seconds after every 50th update.' . nl
		. nl
		. 'Show extra information about what is happening, and don' . sq . 't actually' . nl
		. 'update the rows until you' . sq . 're happy with the results.' . nl
		. '(and you remove --dry-run)' . nl
		;

	return;
	}

# Wrap descriptions at 32 characters and indent subsequent lines 5 tabs.
sub					full_help
	{
        print nl
		. tab . '--debug'				. tabx4	. 'Make debugging easier.'		. nl
		. tab . '--dry-run'				. tabx3	. 'Show what would have been done.'	. nl
		. tab . '--end-value=n'				. tabx3	. 'Ending id-column value.'		. nl
		. tab . '--examples'				. tabx3	. 'Show example commands.'		. nl
		. tab . '--limit=n'				. tabx3	. 'Limit select to n rows.'		. nl
		. tab . '--offset=n'				. tabx3	. 'Offset select by n rows.'		. nl
		. tab . '--port=<port-number>'			. tabx2	. 'Set the port number.'		. nl
		. tab . '--regex-file=<regex-file-name>'	. tab	. 'Specify a file of regexes.'		. nl
		. tab . '--rlike=<regex>'			. tabx3	. 'Select rows matching <regex>.'	. nl
		. tab . '--select-size=n'			. tabx3	. 'Set the select size.'		. nl
		. tab . '--sleep=n'				. tabx3	. 'Sleep n seconds between update'	. nl
								. tabx5	. 'chunks.'				. nl
		. tab . '--squish'				. tabx3	. 'Squish whitespace.'			. nl
		. tab . '--start-value=n'			. tabx3	. 'Starting id-column value.'		. nl
		. tab . '--update-size=n'			. tabx3	. 'Set the update size.'		. nl
		. tab . '--verbose'				. tabx3	. 'Show activity.'			. nl
		. tab . '--word-match'				. tabx3	. 'Match whole words only.'		. nl
		;
	return;
	}

sub					future_help
	{
	print 'Replace in column'										. nlx2
		. tab . '--from-case-insensitive'		. tabx2	. 'Search without regard to case.'	. nl
		. tab . '--leet'				. tabx4	. 'Include numbers when exploding'	. nl
								. tabx5	. 'regexes.'				. nl
		. tab . '--login-path=<login-path>'		. tab	. 'Set the login path.'			. nl
		. tab . '--protocol=<protocol-name>'		. tab	. 'Set the connection protocol.'	. nl
		. tab . '--to-case-insensitive'			. tabx2	. 'Replace without regard to case.'	. nl
		;
	return;
	}

sub					help
	{
	print 'Replace in column'										. nlx2
		. tab . '--help'				. tabx4	. 'Basic usage.'			. nl
		. tab . '--full-help'				. tabx3	. 'More detailed help.'			. nl
		. tab . '--future-help'				. tabx3	. 'Unimplemented features...'		. nl
		. nl
		. tab . '--database=<database-name>'		. tab	. 'Set the database.'			. nl
		. tab . '--host=<host-name>'			. tabx2	. 'Set the host.'			. nl
		. tab . '--password=<password>'			. tabx2	. 'Set the password.'			. nl
		. tab . '--user=<user-name>'			. tabx2	. 'Set the user.'			. nl
		. nl
		. tab . '--id-column=<column-name>'		. tab	. 'Set the ID column name.'		. nl
		. tab . '--regex=<regex>'			. tabx3	. 'Specify a regular expression to'	. nl
								. tabx5	. 'apply to the text column.'		. nl
		. tab . '--table=<table-name>'			. tabx2	. 'Set the table name.'			. nl
		. tab . '--text-column=<column-name>'		. tab	. 'Set the text column name.'		. nl
		;
	return;
	}

########################################################################
# main
########################################################################

# define our command line options
GetOptions(
# help
	  'help'			=> \my $help
	, 'full-help'			=> \my $full_help
	, 'future-help'			=> \my $future_help

	, 'database=s'			=> \my $database
	, 'host=s'			=> \my $host
	, 'password=s'			=> \my $password
	, 'port=s'			=> \my $port
	, 'user=s'			=> \my $user

	, 'id-column=s'			=> \my $id_column
	, 'regex-file=s'		=> \my $regex_file
	, 'regex=s'			=> \my @regexes
	, 'table=s'			=> \my $table
	, 'test'			=> \my $test
	, 'text-column=s'		=> \my $text_column

# full-help
	, 'debug'			=> \$debug
	, 'dry-run'			=> \my $dry_run
	, 'end-value=n'			=> \my $end_value
	, 'examples'			=> \my $examples
	, 'limit=n'			=> \my $limit
	, 'offset=n'			=> \my $offset
	, 'rlike=s'			=> \my $rlike
	, 'select-size=n'		=> \my $select_size
	, 'sleep=n'			=> \my $sleep
	, 'squish'			=> \my $squish
	, 'start-value=n'		=> \my $start_value
	, 'update-size=n'		=> \my $update_size
	, 'verbose'			=> \$verbose
	, 'word-match'			=> \my $word_match

# future-help
	, 'login-path=s'		=> \my $login_path
	, 'protocol=s'			=> \my $protocol
	);

# my cruft
#$DB::single = 1;
	if	($test)
		{
		$database = 'test'		unless $database;
		$host = 'db10'			unless $host;
		$id_column = 'my_id'		unless $id_column;
		$password = 'test'		unless $password;
		$rlike = 'sleep'		unless $rlike;
		$select_size = 5		unless $select_size;
		$sleep = 3			unless $sleep;
		$table = 'sedwards'		unless $table;
		$text_column = 'foo'		unless $text_column;
		$update_size = 5		unless $update_size;
		$user = 'sedwards'		unless $user;
		$verbose = 1;
		@regexes = ('sleep:rest')	unless @regexes;
		@regexes = ('sleeping:napping', 'slept:rested', 'sleep:rest') unless @regexes;
#		$dry_run = 1;
#		$limit = 2			unless $limit;
		}

# need examples?
	if	($examples)
		{
		examples;
		print nl;
		exit(0);
		}

# need help?
	if	($help)
		{
		help;
		print nl;
		exit(0);
		}

# need more help?
	if	($full_help)
		{
		help;
		full_help;
		print nl;
		exit(0);
		}

# dreaming of more help?
	if	($future_help)
		{
		future_help;
		print nl;
		exit(0);
		}

# handle meta options
#$DB::single = 1;
	$select_size = 0		unless $select_size;
	$update_size = 0		unless $update_size;
	if	($dry_run)
		{
		$verbose++;
		}
	$offset = 0			unless $offset;
	$port = '3306'			unless $port;
	$protocol = 'tcp'		unless $protocol;
	if	($word_match)
		{
		$word_match = '\b';
		}
	else
		{
		$word_match = '';
		}

# sanity checks
	my $errors = '';
	if	(!$login_path)
		{
		$errors .= 'Please specify --host.' . nl unless $host;
		$errors .= 'Please specify --user.' . nl unless $user;
		}
	$errors .= 'Please specify --database.' . nl unless $database;
	$errors .= 'Please specify --table.' . nl unless $table;
	$errors .= 'Please specify --id-column.' . nl unless $id_column;
	$errors .= 'Please specify --text-column.' . nl unless $text_column;
	$errors .= 'Please specify --regex.' . nl unless $#regexes >= 0;
	if	($errors)
		{
		print $errors;
		exit;
		}

# connect to MySQL
# (no whitespace allowed)
#$DB::single = 1;
	my $dsn = 'DBI:mysql:'
		. 'database='			. $database . ';'
		. 'host='			. $host . ';'
		. 'port='			. $port . ';'
		. 'protocol='			. $protocol . ';'
#		. 'mysql_client_found_rows='	. '0' . ';'
		;	
	$password =~ tr/\'//d if $password;
	$dbh = DBI->connect(
		  $dsn
		, $user
		, $password
		, {
			  AutoCommit => 1
			, PrintError => 1
			, RaiseError => 1
		}
		);

# read the regex file
#$DB::single = 1;
	if	($regex_file)
		{
		open(my $fh, '<', $regex_file)
			or die 'Failed to read regex file: ' . $! . nl;
		my @regexes_from_file = <$fh>;
		close($fh);
		chomp(@regexes_from_file);
		@regexes = (@regexes_from_file, @regexes);
		}

# split the regexes array and include uc, ucfirst, and lc variants
#$DB::single = 1;
	my @from_regexes;
	my @to_regexes;
	my $join_character = ' ';
#	if	($word_match)
#		{
#		$join_character = $word_match;
#		}
	for	(my $idx = 0; $idx <= $#regexes; $idx++)
		{
		my ($from, $to) = split(':', $regexes[$idx]);
#		push @from_regexes, (uc $from, ucfirst lc $from, lc $from);
#		push @to_regexes, (uc $to, ucfirst lc $to, lc $to);
		push @from_regexes
				, (
				  join(' ', map(uc,         split(' ', ($from))))
				, join(' ', map(ucfirst lc, split(' ', ($from))))
				, join(' ', map(lc,         split(' ', ($from))))
				);
		push @to_regexes
				, (
				  join(' ', map(uc,         split(' ', ($to))))
				, join(' ', map(ucfirst lc, split(' ', ($to))))
				, join(' ', map(lc,         split(' ', ($to))))
				);
		}
# squish whitespace
	if	($squish)
		{
		unshift @from_regexes, ('\s+');
		unshift @to_regexes, (' ');
		}

# prevent parallel execution
#$DB::single = 1;
	$ssh = $dbh->prepare('select get_lock(?, 0)');
	$ssh->execute('replace-in-column');
	@row = $ssh->fetchrow_array();
	if	(1 != $row[0])
		{
		print 'Another instance is already running.'
			. nl
			;
		exit(1);
		}

# get the min and max id values
#$DB::single = 1;
	$ssh = $dbh->prepare(
			  'select min('
			. $id_column
			. '), max('
			. $id_column
			. ') from '
			. $table
			);
	$ssh->execute();
	my ($id_min_value, $id_max_value) = $ssh->fetchrow_array();
	$id_min_value = $start_value if $start_value;
	$id_max_value = $end_value if $end_value;
	$select_size = $id_max_value unless $select_size;

# build the select statement
#$DB::single = 1;
	$select = 'select '
#		. 'SQL_CALC_FOUND_ROWS '
		. $id_column
		. ', '
		. $text_column
		. ' from '
		. $table
		. ' where '
		. $id_column
		. ' >= ? and '
		. $id_column
		. ' < ?'
		;
	$select .= ' and ' . $text_column . ' regexp ' . sq . $rlike . sq if $rlike;
	$select .= ' limit ' . $limit if $limit;
	$select .= ' offset ' . $offset if $offset;
	say 'The select statement is:'
		. nl
		. tab
		. $select
		if $verbose;
	$ssh = $dbh->prepare($select);

# build the update statement
#$DB::single = 1;
	$update = 'update '
		. $table
		. ' set '
		. $text_column
		. ' = ? where '
		. $id_column
		. ' = ?'
		;
	say 'The update statement is:'
		. nl
		. tab
		. $update
		if $verbose;
	$ush = $dbh->prepare($update);

# if we have rows
#$DB::single = 1;
	my $update_counter = ($update_size - 1);
	while	(my $status = $ssh->execute($id_min_value, ($id_min_value + $select_size)))
		{
#$DB::single = 1;
		say 'New select chunk (from '
			. $id_min_value
			. ' to '
			. ($id_min_value + $select_size)
			. ') selected.'
			if $verbose;
		$ssh->bind_columns(\my $id_column_value, \my $text_column_value);
# for each row
		while	($ssh->fetchrow_arrayref)
			{
			my $changed;
			last if ($id_column_value > $id_max_value);
			if	($verbose)
				{
				say nl
					. 'Before:'
					. nl
					. tab
					. $id_column
					. ' = '
					. sq
					. $id_column_value
					. sq
					. nl
					. tab
					. $text_column
					. ' ='
					;
				my $temp = $text_column_value;
				while	($temp =~ /.{1,64}/)
					{
					say tabx2 . $&;
					$temp = $';	# unconfuse emacs '
					}
				}
# for each regex
			for	(my $idx = 0; $idx <= $#from_regexes; $idx++)
				{
#$DB::single = 1;
				say tab
					. 'Applying s/' 
					. $word_match
					. $from_regexes[$idx]
					. $word_match
					. '/'
					. $to_regexes[$idx]
					. '/g'
					if $verbose;
				$changed
					+= $text_column_value
					=~ s/$word_match$from_regexes[$idx]$word_match/$to_regexes[$idx]/g
					;
				}
# any changes?
			if	($changed)
				{
				if	($verbose)
					{
					say 'After:';
					my $temp = $text_column_value;
					while	($temp =~ /.{1,64}/)
						{
						say tabx2 . $&;
						$temp = $';	# unconfuse emacs '
						}
					}
				if	(!$dry_run)
					{
					say 'Updating.' if $verbose;
					$ush->execute($text_column_value, $id_column_value);
					}
				if	($update_size and (!$update_counter--))
					{
					$update_counter = ($update_size - 1);
					say 'Sleeping ' . $sleep . ' seconds.' if $verbose;
					sleep $sleep;
					}
				}
			else
				{
				say 'Unchanged:' if $verbose;
				}
			}
		$id_min_value += $select_size;
		last if ($id_min_value > $id_max_value);
		}

# (end of replace-in-column.pl)
