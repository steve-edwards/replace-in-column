# replace-in-column

This command line utility replaces old-text or regex in a column with
new-text. MySQL specific.

	--help				Basic usage.
	--full-help			More detailed help.
	--future-help			Unimplemented features...

	--database=<database-name>	Set the database.
	--host=<host-name>		Set the host.
	--password=<password>		Set the password.
	--user=<user-name>		Set the user.

	--id-column=<column-name>	Set the ID column name.
	--regex=<regex>			Specify a regular expression to
					apply to the text column.
	--table=<table-name>		Set the table name.
	--text-column=<column-name>	Set the text column name.

	--debug				Make debugging easier.
	--dry-run			Show what would have been done.
	--end-value=n			Ending id-column value.
	--examples			Show an example command.
	--limit=n			Limit select to n rows.
	--offset=n			Offset select by n rows.
	--port=<port-number>		Set the port number.
	--regex-file=<regex-file-name>	Specify a file of regexes.
	--rlike=<regex>			Select rows matching <regex>.
	--select-size=n			Set the select size.
	--sleep=n			Sleep n seconds between update
					chunks.
	--squish			Squish whitespace.
	--start-value=n			Starting id-column value.
	--update-size=n			Set the update size.
	--verbose			Show activity.
	--word-match			Match whole words only.

# (end of README.md)
