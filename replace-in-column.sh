#!/bin/bash

	database=neil
	password=foo
	timestamp=$(date +%F-%H-%M-%S)
	user=sedwards

# <table-name>:<id-column>:<text-column>
# Producer{Clip,Image,Video}Site.{Bottom,Description,Keywords.Title,Top}
# StoreProducer{Clip,Image,Video}s.(ClipDescription,ClipKeywords,ClipTitle}
	table_columns+=' ProducerClipSite:SiteID:Bottom'
	table_columns+=' ProducerClipSite:SiteID:Description'
	table_columns+=' ProducerClipSite:SiteID:Keywords'
	table_columns+=' ProducerClipSite:SiteID:Title'
	table_columns+=' ProducerClipSite:SiteID:Top'

	table_columns+=' ProducerImageSite:SiteID:Bottom'
	table_columns+=' ProducerImageSite:SiteID:Description'
	table_columns+=' ProducerImageSite:SiteID:Keywords'
	table_columns+=' ProducerImageSite:SiteID:Title'
	table_columns+=' ProducerImageSite:SiteID:Top'

	table_columns+=' ProducerVideoSite:SiteID:Bottom'
	table_columns+=' ProducerVideoSite:SiteID:Description'
	table_columns+=' ProducerVideoSite:SiteID:Keywords'
	table_columns+=' ProducerVideoSite:SiteID:Title'
	table_columns+=' ProducerVideoSite:SiteID:Top'

	table_columns+=' StoreProducerClips:ClipID:ClipDescription'
	table_columns+=' StoreProducerClips:ClipID:ClipKeywords'
	table_columns+=' StoreProducerClips:ClipID:ClipTitle'

	table_columns+=' StoreProducerImages:ImageID:ImageDescription'
	table_columns+=' StoreProducerImages:ImageID:ImageKeywords'
	table_columns+=' StoreProducerImages:ImageID:ImageTitle'

	table_columns+=' StoreProducerVideos:VideoID:VideoDescription'
	table_columns+=' StoreProducerVideos:VideoID:VideoKeywords'
	table_columns+=' StoreProducerVideos:VideoID:VideoTitle'

	for	temp in ${table_columns}
		do
		table=${temp%%:*}
		temp=${temp#*:}
		id=${temp%%:*}
		text=${temp#*:}

#		printf 'Dumping %s\n' ${table}
#		mysqldump\
#			--user=${user}\
#			--password=${password}\
#			--databases ${database}\
#			--tables ${table}\
#			>${table}.sql-${timestamp}

		printf 'Updating %s.%s\n' ${table} ${text}
		./replace-in-column.pl\
			--host=localhost\
			--user=${user}\
			--password=${password}\
			--database=${database}\
			--table=${table}\
			--id-column=${id}\
			--text-column=${text}\
			--rlike='make love not war|mlnw'\
			--regex='make love not war:make love'\
			--regex=mlnw:ml\
			--sleep=2\
			--verbose\
			--update-size=50\
			--select-size=2000\
			--dry-run\
			--word-match
			done
exit

# (end of replace-in-column.sh)
